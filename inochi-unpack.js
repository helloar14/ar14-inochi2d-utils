
let fs = require('fs');

let magicBytesCode = '';
let jsonPayloadLength;
let texSectHeader = '';
let textureCount;
let textureBlobs = [];
let extSectHeader = '';
let extPayloadCount;
let extSectBlobs = [];

let processedTextureBlobCount = 0;
let processedEXTSectionCount = 0;

let currTextureBlobLength = 0;
let currTextureBlobEncoding = 0;

let currExtPayloadName = '';
let currExtPayloadNameLength = 0;
let currExtPayloadLength = 0;

function readMagicBytes(inputFilePath) {

    magicBytes = fs.createReadStream(inputFilePath, { start: 0, end: 7 });
      
    magicBytes.on('data', function(chunk) { magicBytesCode += chunk.toString(); });

    magicBytes.on('close', function() {

        console.log('Magic bytes code: ' + magicBytesCode);

        if (magicBytesCode != 'TRNSRTS\0') { 

            console.error('Invalid magic bytes code! Unpacking process aborted.'); return;

        } else {

            readWriteJSONPayload(inputFilePath);

        }

    });

}

function readWriteJSONPayload(inputFilePath) {

    jsonPayloadLength = fs.createReadStream(inputFilePath, { start: 8, end: 11 });
      
    jsonPayloadLength.on('data', function(chunk) { jsonPayloadLength = chunk.readUIntBE(0, 4); });

    jsonPayloadLength.on('close', function() {

        console.log('JSON payload length: ' + jsonPayloadLength);

        jsonPayload = fs.createReadStream(inputFilePath, { start: 12, end: jsonPayloadLength + 11 });
        jsonPayloadOutput = fs.createWriteStream('./output/config.json');

        jsonPayload.pipe(jsonPayloadOutput);

        jsonPayloadOutput.on('close', function() {

            console.log('JSON payload exported to config.json');

            readTexSectHeader(inputFilePath, jsonPayloadLength + 12);

        });

    });

}

function readTexSectHeader(inputFilePath, startbyte) {

    texSect = fs.createReadStream(inputFilePath, { start: startbyte, end: startbyte + 7 });
      
    texSect.on('data', function(chunk) { texSectHeader += chunk.toString(); });

    texSect.on('close', function() {

        console.log('Texture section header: ' + texSectHeader);

        if (texSectHeader != 'TEX_SECT') { 

            console.error('Invalid header code! Unpacking process aborted.'); return;

        } else {

            readTextureCount(inputFilePath, startbyte + 8);

        }

    });

}

function readTextureCount(inputFilePath, startbyte) {

    texCount = fs.createReadStream(inputFilePath, { start: startbyte, end: startbyte + 3 });
      
    texCount.on('data', function(chunk) { textureCount = chunk.readUIntBE(0, 4); });

    texCount.on('close', function() {

        console.log('Texture count: ' + textureCount);

        readWriteTextureBlobs(inputFilePath, startbyte + 4);

    });

}

function readWriteTextureBlobs(inputFilePath, startbyte) {

    texLength = fs.createReadStream(inputFilePath, { start: startbyte, end: startbyte + 3 });
      
    texLength.on('data', function(chunk) { currTextureBlobLength = chunk.readUIntBE(0, 4); });

    texLength.on('close', function() {

        console.log('Texture ID #' + processedTextureBlobCount + ' length: ' + currTextureBlobLength);

        texEncoding = fs.createReadStream(inputFilePath, { start: startbyte + 4, end: startbyte + 4 });
      
        texEncoding.on('data', function(chunk) { currTextureBlobEncoding = chunk.readUIntBE(0, 1); });

        texEncoding.on('close', function() {

            console.log('Texture ID #' + processedTextureBlobCount + ' encoding: ' + currTextureBlobEncoding);

            texExt = '.png';

            if (currTextureBlobEncoding == 1) { texExt = '.tga'; } else if (currTextureBlobEncoding == 2) { texExt = '.bc7'; }

            texBlob = fs.createReadStream(inputFilePath, { start: startbyte + 5, end: currTextureBlobLength + startbyte + 4 });
            texBlobOutput = fs.createWriteStream('./output/texture' + processedTextureBlobCount + texExt);

            texBlob.pipe(texBlobOutput);

            texBlobOutput.on('close', function() {

                console.log('Texture ID #' + processedTextureBlobCount + ' exported.');

                processedTextureBlobCount++;

                if (processedTextureBlobCount < textureCount) {

                    readWriteTextureBlobs(inputFilePath, currTextureBlobLength + startbyte + 5);

                } else {

                    readExtSectHeader(inputFilePath, currTextureBlobLength + startbyte + 5);

                }

            });

        });

    });

}

function readExtSectHeader(inputFilePath, startbyte) {

    extSect = fs.createReadStream(inputFilePath, { start: startbyte, end: startbyte + 7 });
      
    extSect.on('data', function(chunk) { extSectHeader += chunk.toString(); });

    extSect.on('close', function() {

        console.log('EXT section header: ' + extSectHeader);

        if (texSectHeader != '') { 

            console.log('No EXT header found.'); return;

        } else if (texSectHeader != 'EXT_SECT') { 

            console.error('Invalid header code! Unpacking process aborted.'); return;

        } else {

            readExtCount(inputFilePath, startbyte + 8);

        }

    });

}

function readExtCount(inputFilePath, startbyte) {

    extCount = fs.createReadStream(inputFilePath, { start: startbyte, end: startbyte + 3 });
      
    extCount.on('data', function(chunk) { extPayloadCount = chunk.readUIntBE(0, 4); });

    extCount.on('close', function() {

        console.log('Texture count: ' + extPayloadCount);

        readWriteExtPayloads(inputFilePath, startbyte + 4);

    });

}

function readWriteExtPayloads(inputFilePath, startbyte) {

    extNameLength = fs.createReadStream(inputFilePath, { start: startbyte, end: startbyte + 3 });
      
    extNameLength.on('data', function(chunk) { currExtPayloadNameLength = chunk.readUIntBE(0, 4); });

    extNameLength.on('close', function() {

        extName = fs.createReadStream(inputFilePath, { start: startbyte + 4, end: startbyte + currExtPayloadNameLength + 3 });
      
        extName.on('data', function(chunk) { currExtPayloadName += chunk.toString(); });

        extName.on('close', function() {

            console.log('EXT ID #' + processedEXTSectionCount + ' name: ' + currExtPayloadName);

            extLength = fs.createReadStream(inputFilePath, { start: startbyte + currExtPayloadNameLength + 4, end: startbyte + currExtPayloadNameLength + 7 });
      
            extLength.on('data', function(chunk) { currExtPayloadLength = chunk.readUIntBE(0, 4); });

            extLength.on('close', function() {

                jsonPayload = fs.createReadStream(inputFilePath, { start: startbyte + currExtPayloadNameLength + 8, end: startbyte + currExtPayloadNameLength + currExtPayloadLength + 7 });
                jsonPayloadOutput = fs.createWriteStream('./output/ext' + processedEXTSectionCount + '-' + currExtPayloadName + '.json');

                jsonPayload.pipe(jsonPayloadOutput);

                jsonPayloadOutput.on('close', function() {

                    console.log('JSON payload exported to ext' + processedEXTSectionCount + '-' + currExtPayloadName + '.json');

                    processedEXTSectionCount++;

                    if (processedEXTSectionCount < extCount) {

                       readWriteExtPayloads(inputFilePath, startbyte + currExtPayloadNameLength + currExtPayloadLength + 7);

                    }

                });

            });

        });

    });

}

if (process.argv.length < 3) {

    console.error('No input file specified. Specify the input file by using "node inochi-unpack path/to/inochi2d-file"');

} else {

    if (!fs.existsSync('./output')) { 

        fs.mkdirSync('./output'); 

        readMagicBytes(process.argv[2]);

    } else { 

        fs.rm('./output', { recursive: true }, (err) => {

            if (err) {
                    
                console.error(err.message);
                return;

            }

            fs.mkdirSync('./output'); 

            readMagicBytes(process.argv[2]);

        });

    }

}