# Inochi2D Utils

Inochi2D-related utility scripts. These are not good codes, just something that works for personal use.

## inochi-unpack

Unpack `.inp` or `.inx` file into `./output` folder. Usage: `node inochi-unpack path/to/inochi2d-file`
